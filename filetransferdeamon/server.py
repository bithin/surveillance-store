import os

from twisted.internet import reactor, protocol
from twisted.protocols import basic

from helper import display_message, validate_file_md5_hash, get_file_md5_hash, \
                    read_bytes_from_file

class FileTransferProtocol(basic.LineReceiver):

    def connectionMade(self):
        self.factory.clients.append(self)
        self.file_handler = None
        self.file_path = os.getcwd()
        self.filename_received = False
        self.filehash_received = False
        self.filename = ''
        self.filehash = ''
        self.filedata = ''

    def connectionLost(self, reason):
        self.factory.clients.remove(self)
        self.file_handler = None

    def lineReceived(self, line):
        line = line.strip()
        if not self.filename_received:
            self.filename_received = True
            self.filename = line
            print line
        elif not self.filehash_received:
            self.filehash_received = True
            self.filehash = line
            print line
        else:
            self.setRawMode()

    def rawDataReceived(self, data):
        if not self.file_handler:
            print self.file_path
            self.file_handler = open(self.file_path+'/'+self.filename, 'wb')

        if data.endswith(self.filehash):
            data = data[:-32]
            self.file_handler.write(data)
            self.setLineMode()
            self.file_handler.close()
            self.file_handler = None

            if validate_file_md5_hash(self.file_path+'/'+self.filename, self.filehash):
                print("valid hash")
            else:
                print("Invaild hash")
        else:
            self.file_handler.write(data)

class FileTransferServerFactory(protocol.ServerFactory):
    protocol = FileTransferProtocol

    def __init__(self, file_path):
        self.file_path = file_path
        self.clients = []
        self.files = None


if __name__ == '__main__':
    display_message('Listening on TCP port 54002')
    reactor.listenTCP(54002, FileTransferServerFactory(os.getcwd()))
    reactor.run()

#!/usr/bin/env python

from os import path

import tornado.options
import tornado.web
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options

from netaddr import *

import psycopg2
import json
import socket, struct, sys
import inspect

from async_psycopg2 import Pool
from time import sleep


define('port', default=8888, help='run on the given port', type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', MainHandler),
            (r'/addnode', NodeAddHandler),
            (r'/getnodedetails', NodeDetailsHandler),
            (r'/updatenode', NodeUpdateHandler),
            (r'/addpolicy', PolicyAddHandler),
            (r'/getpolicydetails', PolicyDetailsHandler),
            (r'/updatepolicy', PolicyUpdateHandler),
            (r'/addtenant', TenantAddHandler),
            (r'/gettenantdetails', TenantDetailsHandler),
            (r'/updatetenant', TenantUpdateHandler),
            (r'/addcontainer', ContainerAddHandler),
            (r'/getcontainerdetails', ContainerDetailsHandler),
            (r'/updatecontainer', ContainerUpdateHandler),
            (r'/adddevice', DeviceAddHandler),
            (r'/getdevicedetails', DeviceDetailsHandler),
            (r'/updatedevice', DeviceUpdateHandler),
            (r'/addlog', LogAddHandler),
            (r'/getlogdetails', LogDetailsHandler),
            (r'/updatelog', LogUpdateHandler),
        ]
        settings = dict(
            template_path=path.join(path.dirname(__file__), 'templates'),
            static_path=path.join(path.dirname(__file__), 'static'),
            xsrf_cookies=False,
            cookie_secret='dsfretghj867544wgryjuyki9p9lou67543/Vo=',
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        self.db = None


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        if not self.application.db:
            self.application.db = Pool(1, 20, 10, **{
                'host': 'localhost',
                'database': 'iot',
                'user': 'iot_tornado',
                'password': 'amma',
                'async': 1
            })
        return self.application.db


class MainHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self):
        self.write('Sorry we are not providing any service now')

class NodeAddHandler(BaseHandler):
    """
    The node can be either a gateway or a storage medium. This class will add a
    node into the IoT infrastructure.

    :param node_name: A string, the node's name
    :param ip_address: A Inet, the node's ip address
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            node_name = str(self.get_argument('node_name', True))
            ip_address = self.get_argument('ip_address', True)
            storage_status = str(self.get_argument('storage_status', True))
            self.db.execute("insert into nodes (node_name, ip_address, \
                                storage_status) values ('%s', '%s', '%s')" \
                                % (node_name, ip_address, storage_status), \
                                callback=self.add_response)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def add_response(self, cursor):
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class NodeDetailsHandler(BaseHandler):
    """
    To retrive all the details of a particular node in the infrastructure.

    :param node_id: An INT, unique id assigned to a node
    :rtype: JSONObject<node_name, ip_address>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            node_id = int(self.get_argument('node_id', True))
            self.db.execute("select node_name, ip_address from nodes where \
                                node_id=(%d)" % node_id, \
                                callback = self.get_details)
        except ValueError:
            message = 'Unable to fetch the node details'
            self.send_error(400, message=message)

    def get_details(self, cursor):
        results = cursor.fetchall()
        details = {}
        if results:
            details['node_name'] = results[0][0]
            details['ip_address'] = results[0][1]

        self.write(json.dumps(details))
        self.finish()

class NodeUpdateHandler(BaseHandler):
    """
    Update the details of a particular node in the infrastructure. For example,
    the ip address of a node can change or the storage status.

    :param node_id: An INT, unique id assigned to a node
    :rtype: JSONObject<boolean> True on query success
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            node_id = int(self.get_argument('node_id', True))
            node_name = self.get_argument('node_name', True)
            ip_address = self.get_argument('ip_address', True)
            storage_status = self.get_argument('storage_status', True)
            self.db.execute("update nodes set node_name='%s', ip_address='%s', \
                                storage_status='%s' where node_id=%d" % \
                                (node_name, ip_address, storage_status, \
                                node_id), callback = self.update_details)
        except ValueError:
            message = 'Unable to fetch the node details'
            self.send_error(400, message = message)

    def update_details(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()


class PolicyAddHandler(BaseHandler):
    """
    To add the policy details for a tenant. The storage mediums are catagorized
    into different class based on the storage cost. The current policy is only
    based on amount of time it will be stored a particular storage medium. Later
    version will include higher level implementation.

    :param time_at_gold: An INT, duration of storage at In-Memory
    :param time_at_silver: An INT, duration of storage at Fast-SSD
    :param time_at_bronze: An INT, duraion of storage at Disk Drive
    :param time_to_delete: An INT, lifetime of a file
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            time_at_gold = int(self.get_argument('time_at_gold', True))
            time_at_silver = int(self.get_argument('time_at_silver', True))
            time_at_bronze = int(self.get_argument('time_at_bronze', True))
            time_to_delete = int(self.get_argument('time_to_delete', True))
            self.db.execute("insert into policy (time_at_gold, time_at_silver, \
                                time_at_bronze, time_to_delete) values (%d, %d,\
                                %d, %d)" % (time_at_gold, time_at_silver, \
                                time_at_bronze, time_to_delete), \
                                callback = self.add_response)

        except ValueError:
            message = 'Unable to fetch the node details'
            self.send_error(400, message=message)

    def add_response(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()


class PolicyDetailsHandler(BaseHandler):
    """
    To retrive the policy details, that is defined for the IoT infrastructure.

    :param policy_id: An INT, unique id assigned to a policy definitation
    :rtype: A json_object
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            policy_id = int(self.get_argument('policy_id', True))
            self.db.execute("select time_at_gold, time_at_silver, \
                                time_at_bronze, time_to_delete from policy \
                                where policy_id=(%d)" % policy_id, \
                                callback = self.get_details)
        except ValueError:
            message = 'Unable to fetch the node details'
            self.send_error(400, message=message)

    def get_details(self, cursor):
        results = cursor.fetchall()
        details = {}
        if results:
            details['time_at_gold'] = results[0][0]
            details['time_at_silver'] = results[0][1]
            details['time_at_bronze'] = results[0][2]
            details['time_to_delete'] = results[0][3]

        self.write(json.dumps(details))
        self.finish()


class PolicyUpdateHandler(BaseHandler):
    """
    The Tenant can update there policies according to the requirement. Currently
    , they can only specify the duration for which a piece of data can be kept
    in a particular storage medium.

    :param time_at_gold:An INT, duration of storage at In-Memory
    :param time_at_silver: An INT, duration of storage at Fast-SSD
    :param time_at_bronze: An INT, duraion of storage at Disk Drive
    :param time_to_delete: An INT, lifetime of a log chunk
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            policy_id = int(self.get_argument('policy_id', True))
            time_at_gold = int(self.get_argument('time_at_gold', True))
            time_at_silver = int(self.get_argument('time_at_silver', True))
            time_at_bronze = int(self.get_argument('time_at_bronze', True))
            time_to_delete = int(self.get_argument('time_to_delete', True))
            self.db.execute("update policy set time_at_gold = %d, \
                                time_at_silver = %d, time_at_bronze = %d, \
                                time_to_delete = %d where policy_id = %d" \
                                % (time_at_gold, time_at_silver, \
                                time_at_bronze, time_to_delete, policy_id), \
                                callback=self.update_details)

        except ValueError:
            message = 'Unable to fetch the node details'
            self.send_error(400, message=message)


    def update_details(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()


class TenantAddHandler(BaseHandler):
    """
    A Tenant is a user of the IoT infrastructure. There can be multiple Tenants
    using a single IoT infrastructure. The current implementation only support
    defining policy per Tenant.

    :param node_name: A string, the node's name
    :param ip_address: A string, the node's ip address
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            tenant_name = self.get_argument('tenant_name', True)
            company = self.get_argument('company', True)
            policy_id = int(self.get_argument('policy_id', True))
            self.db.execute("insert into tenant (tenant_name, company, \
                                 policy_id) values ('%s', '%s', %d)"% \
                                 (tenant_name, company, policy_id), \
                                 callback=self.add_response)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def add_response(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class TenantDetailsHandler(BaseHandler):
    """
    To retrive details of a specific Tenant, if the Tenant id is given

    :param tenant_id: An INT, unique idenity for a Tenant
    :rtype: A JSONObject<tanant_name, company, policy_id>
    """
    @tornado.web.asynchronous
    def post(self):
        try:
            tenant_id = int(self.get_argument('tenant_id', True))
            self.db.execute("select tenant_name, company, policy_id from tenant \
                             where tenant_id=(%d)" % tenant_id, \
                             callback = self.get_details)
        except ValueError:
            message = 'Unable to fetch the node details'
            self.send_error(400, message=message)

    def get_details(self, cursor):
        results = cursor.fetchall()
        details = {}
        if results:
            details['tenant_name'] = results[0][0]
            details['company'] = results[0][1]
            details['policy_id'] = results[0][2]

        self.write(json.dumps(details))
        self.finish()

class TenantUpdateHandler(BaseHandler):
    """
    To update the details of a tenant

    :param tenant_id: An INT, unique id for tenant
    :param company: A String, represent the name of the company
    :param policy: An INT, policy details associated with the tenant
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            tenant_id = int(self.get_argument('tenant_id', True))
            tenant_name = self.get_argument('tenant_name', True)
            company = self.get_argument('company', True)
            policy_id = int(self.get_argument('policy_id', True))
            self.db.execute("update tenant set tenant_name = '%s', \
                                company = '%s', policy_id = %d where \
                                tenant_id = %d" % (tenant_name, company, \
                                policy_id, tenant_id), \
                                callback = self.update_details)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def update_details(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class ContainerAddHandler(BaseHandler):
    """
    Data will be stored in container and then migrated across the IoT infrastru-
    cture. The containers are created either in the gateway or storage medium.
    The containers can be assigned an ip address and define storage size.

    :param container_id: A INT, unique identifer for the container
    :param ip_address: A String, address of the container
    :param tenant_id: An INT, to which Tenant the container belong
    :param size: An INT, storage capacity of the container
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            container_id = self.get_argument('container_id', True)
            ip_address = self.get_argument('ip_address', True)
            tenant_id = int(self.get_argument('tenant_id', True))
            size = int(self.get_argument('size', True))
            node_id = int(self.get_argument('node_id', True))
            self.db.execute("insert into container (container_id, storage_size, \
                                node_id,  tenant_id, ip_address) values ('%s', \
                                %d, %d, %d, '%s')"% (container_id, size, \
                                node_id, tenant_id, ip_address), \
                                callback = self.add_response)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def add_response(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class ContainerDetailsHandler(BaseHandler):
    """
    To retrive details of a specific container.

    :param container_id: An String, unique idenity for a Container
    :rtype: A JSONObject<ip_address, tenant_id, size>
    """

    @tornado.web.asynchronous
    def post(self):
        container_id = self.get_argument('container_id', True)
        self.db.execute("select ip_address, storage_size, tenant_id, node_id from \
                            container where container_id='%s'" % container_id, \
                            callback = self.get_details)

    def get_details(self, cursor):
        results = cursor.fetchall()
        details = {}
        if results:
            details['ip_address'] = results[0][0]
            details['size'] = results[0][1]
            details['tenant_id'] = results[0][2]
            details['node_id'] = results[0][3]

        self.write(json.dumps(details))
        self.finish()

class ContainerUpdateHandler(BaseHandler):
    """
    To update the details of a Container

    :param container_id: An String, unique id for Container
    :param updated_container_id: An INT, new id for the container
    :param ip_address: A String, address for a container
    :param size: An INT, storage capacity of the container
    :param tenant_id: An INT, specify to which tenant the container belong
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            container_id = self.get_argument('container_id', True)
            update_container_id = self.get_argument('update_container_id', True)
            ip_address = self.get_argument('ip_address', True)
            size = int(self.get_argument('size', True))
            tenant_id = int(self.get_argument('tenant_id', True))
            self.db.execute("update container set container_id = '%s', \
                                ip_address = '%s', storage_size = %d, \
                                tenant_id = %d where container_id = '%s'"% \
                                (update_container_id, ip_address, size, \
                                tenant_id, container_id), \
                                callback = self.update_details)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def update_details(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class DeviceAddHandler(BaseHandler):
    """
    An IoT device which can genarate data, in this case, ip cameras.

    :param device_id: A String, unique identifer for the device
    :param ip_address: A String, address of the device
    :param device_type: A String, the type of the device ie sensor, cameras etc
    :param device_name: A String, the name of the device
    :param container_id: An String, to which Container the device is connected
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            device_id = self.get_argument('device_id', True)
            ip_address = self.get_argument('ip_address', True)
            device_type = self.get_argument('device_type', True)
            device_name = self.get_argument('device_name', True)
            container_id = self.get_argument('container_id', True)
            self.db.execute("insert into device (device_id, ip_address, \
                                 device_type, device_name, container_id) \
                                 values ('%s', '%s', '%s', '%s', '%s')"% \
                                 (device_id, ip_address, device_type, \
                                 device_name, container_id), \
                                 callback = self.add_response)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def add_response(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class DeviceDetailsHandler(BaseHandler):
    """
    To retrive details of a specific container.

    :param device_id: An String, unique idenity for a Device
    :rtype: A JSONObject<ip_address, device_type, device_name, container_id>
    """

    @tornado.web.asynchronous
    def post(self):
        device_id = self.get_argument('device_id', True)
        self.db.execute("select ip_address, device_type, device_name, \
                            container_id from device where device_id=('%s')" % \
                            device_id, callback = self.get_details)

    def get_details(self, cursor):
        results = cursor.fetchall()
        details = {}
        if results:
            details['ip_address'] = results[0][0]
            details['device_type'] = results[0][1]
            details['device_name'] = results[0][2]
            details['container_id'] = results[0][3]

        self.write(json.dumps(details))
        self.finish()

class DeviceUpdateHandler(BaseHandler):
    """
    To update the details of a Device

    :param device_id: An INT, unique id for Device
    :param ip_address: A String, address for a Device
    :param device_type: A String, whether it is gateway or storage node
    :param device_name: A String, name of the connected device
    :param container_id: A String, unique id for a container
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            device_id = self.get_argument('device_id', True)
            ip_address = self.get_argument('ip_address', True)
            device_type = self.get_argument('device_type', True)
            device_name = self.get_argument('device_name', True)
            container_id = self.get_argument('container_id', True)
            self.db.execute("update device set ip_address = ('%s'), \
                                device_type = ('%s'), device_name = ('%s'), \
                                container_id = ('%s') where device_id = ('%s')"% \
                                (ip_address, device_type, device_name, \
                                container_id, device_id), \
                                callback = self.update_details)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def update_details(self, cursor):
        status = {}
        status['valid'] = False

        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class LogAddHandler(BaseHandler):
    """
    The data in the IoT infrastructure is represent as logs. Each log is divided
    into different chunks and then spread across different storage medium for
    efficient storage and archival.

    :param log_id: A String, unique identifer for the log chunk
    :param parent_id: A String, to which log this log chunk belongs
    :param container_id: An String, in which container the log is stored
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            log_id = self.get_argument('log_id', True)
            parent_id = self.get_argument('parent_id', None)
            container_id = self.get_argument('container_id', True)
            child_id = self.get_argument('child_id', None)
            self.db.execute("insert into log (log_id, parent_id, child_id, \
                                 container_id) values ('%s', '%s', '%s', '%s')"
                                 %(log_id, parent_id, child_id, container_id), \
                                 callback = self.add_response)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def add_response(self, cursor):
        status = {}
        status['valid'] = False
        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()

class LogDetailsHandler(BaseHandler):
    """
    To retrive details of a specific container.

    :param log_id: An String, unique idenity for a Log chunk
    :rtype: A JSONObject<parent_id, container_id>
    """
    @tornado.web.asynchronous
    def post(self):
        log_id = self.get_argument('log_id', True)
        self.db.execute("select parent_id, child_id, container_id from log \
                            where log_id=('%s')" % log_id, \
                            callback = self.get_details)

    def get_details(self, cursor):
        details = {}
        results = cursor.fetchall()
        if results:
            details['parent_id'] = results[0][0]
            details['container_id'] = results[0][1]

        self.write(json.dumps(details))
        self.finish()

class LogUpdateHandler(BaseHandler):
    """
    To update the details of a Device

    :param log_id: An String, unique id for Log chunk
    :param parent_id: A String, address of the parent log chunk
    :param container_id: A String, in which container the log is stored
    :rtype: JSONObject<boolean>
    """

    @tornado.web.asynchronous
    def post(self):
        try:
            log_id = self.get_argument('log_id', True)
            parent_id = self.get_argument('parent_id', True)
            container_id = self.get_argument('container_id', True)
            self.db.execute("update log set parent_id = ('%s'), \
                                container_id = ('%s') where log_id = ('%s')" % \
                                (parent_id, container_id, log_id), \
                                callback = self.update_details)
        except ValueError:
            message = 'Unable to handle the request'
            self.send_error(400, message=message)

    def update_details(self, cursor):
        status = {}
        status['valid'] = False

        if cursor:
            status['valid'] = True

        self.write(json.dumps(status))
        self.finish()


def main():
    sys.backtracelimit = 1000
    try:
        tornado.options.parse_command_line()
        http_server = HTTPServer(Application())
        http_server.bind(8888)
        http_server.start(0) # Forks multiple sub-processes
        IOLoop.instance().start()
    except KeyboardInterrupt:
        print 'Exit'


if __name__ == '__main__':
    main()

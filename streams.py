import sys
import live555
import random
import os
import pickle

from threading import Thread, Lock
from time import strftime, localtime, sleep, time
from math import floor
from helper import PolicyInfo, HostInfo, Trigger, Log, Container

stream_buffer = []
current_filesize = 0
total_size = 0
lock = Lock()
stoptime = time()+10


class AcceptStream(Thread):
    """
    Accept RTSP streams from the security cameras and push into a stream buffer.
    The stream buffer will be read by another module and written it into a file.
    The file will be divided into different chunks and transported across the
    network finally into a storage medium.
    """

    def __init__(self, stream_url, use_tcp):
        super(AcceptStream, self).__init__()
        self.video_stream_url = stream_url
        self.use_tcp = use_tcp

    def run(self):
        self.start_stream()

    def start_stream(self):
        live555.startRTSP(self.video_stream_url, self.read_video_frame, \
                            self.use_tcp)
        t = Thread(target=live555.runEventLoop, args=())
        t.setDaemon(True)
        t.start()
        t.join()

    def read_video_frame(self, codecName, bytes, sec, usec, durUSec):
        lock.acquire()
        global stream_buffer
        global current_filesize
        global total_size

        if time() > stoptime:
            lock.release()
            sleep(0.5)
            return

        stream_buffer.append(bytes)
        current_filesize = current_filesize + len(bytes)
        total_size = total_size + len(bytes)
        lock.release()
        sleep(random.random())

class CreateLog(Thread):
    """
    The received streams are written into a log file. The log file will be divi-
    ded into different chunk. The chunks are named with a 256 bits string and
    the details are updated to the metadata server.
    """

    def __init__(self, log_filename, log_prev, filesize, tenant_id, \
                    container_id, port, tenant_url, policy_url, log_url, \
                    device_id):
        super(CreateLog, self).__init__()
        self.log_filename = log_filename
        self.log_prev = log_prev
        self.filesize = filesize
        self.tenant_url = tenant_url
        self.policy_url = policy_url
        self.log_url = log_url
        self.tenant_id = tenant_id
        self.container_id = container_id
        self.port = port
        self.device_id = device_id

    def run(self):
        self.read_stream_buffer()

    def read_stream_buffer(self):
        policy = PolicyInfo(self.tenant_id, self.tenant_url, \
				self.policy_url, self.log_filename, self.device_id)
        host = HostInfo()
        trigger_migration = Trigger(self.port, host.get_host_details(), \
                                        self.container_id)
        log_obj = Log(self.container_id, self.log_url)
        while True:
            lock.acquire()
            global current_filesize
            global stoptime
            if time() > stoptime:
                status = log_obj.add_log(self.log_filename, self.log_prev)
                if not status:
                    print("Unable to update the log details")
                    break

                lock.release()
                trigger_migration.send_signal()
                break

            if stream_buffer:
                if current_filesize > self.filesize:
                    status = log_obj.add_log(self.log_filename, self.log_prev)
                    if not status:
                        print("Unable to update the log")
                        break
                    self.log_prev = self.log_filename
                    self.log_filename = self.device_id+'_'+ \
                                            str(random.getrandbits(256))
                    fOut = open(self.log_filename, 'ab')
                    policy_details = policy.get_policy_details()
                    header = pickle.dumps(policy_details)
                    fOut.write(header)
                    current_filesize = 0
                else:
                    fOut = open(self.log_filename, 'ab')

                for i in stream_buffer:
                    fOut.write(b'\0\0\0\1' + i)
                    stream_buffer.pop()
                fOut.close()

            lock.release()
            sleep(random.random())
